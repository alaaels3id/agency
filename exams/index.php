<?php
    session_start();
    if(!isset($_SESSION['username'])){
        include '../inc/controller.php';
        die();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cairo High Institute For Computer Science</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Theme CSS -->
    <link href="../css/agency.css" rel="stylesheet">
    <style type="text/css">
    form .form-group textarea{ resize: none; height: 250px;}
    form .form-group h3{ text-transform: none; float: left; font-family: 'Serge UI Light', 'Serge UI'; font-size: 20px;}
    form .form-group h4{ text-transform: none; float: left; font-family: 'Serge UI Light', 'Serge UI'; font-size: 18px; padding-top: 5px;}
    form input[name='submit']{ float: left; width: 200px;}
    form .col-md-12{ text-align: left; display: inline;}
    form .col-md-12 .form-group{ display: inline; width: 500px;}
    form .col-md-12 input{ float: left;}
    </style>
    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="index">
    <?php include '../inc/nav.php'; ?>
    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome to CHI System!</div>
                <div class="intro-heading">It's Nice To Meet You</div>
                <a href="#exam" class="page-scroll btn btn-xl">Tell Me More</a>
            </div>
        </div>
         <?php include '../php/exams.php'; include '../php/exam.php';?>
    </header>
    <!-- Portfolio Grid Section -->
    <section id="exam" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">
                        <?php
                            while($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)){
                                echo "Online Examination on <mark>".$row['exam_subject']."</mark>";
                            }
                        ?>
                    </h2>
                    <h3 class="section-subheading text-muted">This is an online exam</h3>
                </div>
            </div>
            <div class="row text-center">
                <section id="exams">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="post" action="../php/answers.php">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                        <?php
                                        while($row1 = @mysqli_fetch_array($sql1, MYSQLI_ASSOC))
                                        {
                                            echo '<fieldset style="background-color: #fff; color: #000; padding: 50px; border-radius: 30px;">';
                                            echo "<h3>{$row1['question']}</h3>";
                                            
                                            echo '<div class="form-group">';
                                            echo "&ensp;<label>{$row1['c1']}</label>"; 
                                            echo "<input type=\"radio\" name=\"{$row1['id']}\" value=\"{$row1['c1']}\">";
                                            echo '</div><br>';

                                            echo '<div class="form-group">';
                                            echo "&ensp;<label>{$row1['c2']}</label>"; 
                                            echo "<input type=\"radio\" name=\"{$row1['id']}\" value=\"{$row1['c2']}\">";
                                            echo '</div><br>';

                                            echo '<div class="form-group">';
                                            echo "&ensp;<label>{$row1['c3']}</label>"; 
                                            echo "<input type=\"radio\" name=\"{$row1['id']}\" value=\"{$row1['c3']}\">";
                                            echo '</div><br>';
                                            
                                            echo '<div class="form-group">';
                                            echo "&ensp;<label>{$row1['c4']}</label>"; 
                                            echo "<input type=\"radio\" name=\"{$row1['id']}\" value=\"{$row1['c4']}\">";
                                            
                                            echo '</div><br>';
                                            echo '</fieldset><br><br>';
                                        }
                                        ?>        
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-lg btn-warning" name="submit" value="answers">
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <?php include '../inc/footer.php'; ?>
    <?php include '../inc/scripts2.php'; ?>
</body>
</html>
