<h1>Students Answers</h1>
<style>
table {margin-top:20px;}
table thead th{ font-size: 20px; font-weight: bold; color: #d12;
  	/*background: linear-gradient(to left, #7b4397, #dc2430);*/
}
</style>
<?php 
	include '../models/database.php';
	$obj = new Database('../models/info.php');
	$conn = $obj->Connect();
	$query = "SELECT * FROM `answers`";
	$sql = @mysqli_query($conn, $query) or die("ERROR : Try Again!");
?>
<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Student Name</th>
			<th>Q1 Answer</th>
			<th>Q2 Answer</th>
			<th>Q3 Answer</th>
			<th>Q4 Answer</th>
			<th>Q5 Answer</th>
		</tr>
	</thead>
	<tbody>
	<?php
		while($row = mysqli_fetch_assoc($sql)){	
			echo
			'
				<tr>
					<td>'.$row['id'].'</td>
					<td>'.ucfirst($row['stu_name']).'</td>
					<td>'.$row['q1_answer'].'</td>
					<td>'.$row['q2_answer'].'</td>
					<td>'.$row['q3_answer'].'</td>
					<td>'.$row['q4_answer'].'</td>
					<td>'.$row['q5_answer'].'</td>
				</tr>
			';
		}
	?>
	</tbody>
</table>