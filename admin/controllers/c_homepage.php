<h1>Home Page</h1>
<?php 
	if(isset($_POST['submit']) && $_POST['submit'] == "Save")
	{	
		$homepagedata['discription'] = $_POST['discription'];
		$myfile = $_POST['lec_name'];
		$file = $_FILES['myfile']['name'];
		$tmp_name = $_FILES['myfile']['tmp_name'];
		$file_extension = @strtolower(end(explode('.', $file)));
		
		try
		{
			$errors = array();
			
			if(empty($errors))
			{
				$ran = rand(0, 30000);
				$homepagedata['file_name'] = $myfile.".".$file_extension;
				$homepagedata['lec_name'] = $ran.$myfile.".".$file_extension;
				$location = "./"."uploads/".$ran.$myfile.".".$file_extension;
				$homepagedata['location'] = $location;
				$muf = move_uploaded_file($tmp_name, $location);
				if($muf)
				{
					include '../models/general.php';
					include '../models/insert.php';
					
					$object = new InsertInto($homepagedata, "home");
					
					if($object == TRUE) echo "<h3>File uploaded</h3>";
				
				}else{
					echo "File not uploaded";
				}
			}else{
				foreach ($errors as $error) {
					echo "<h3>".$error."</h3></br>";
				}
			}
		}catch (Exception $e){
			echo $e->getMessage();
		}
	}else include 'views/homepage.php';
?>