<h1>Doctors Page</h1>
<?php 
	if(isset($_POST['submit']) && $_POST['submit'] == "Save"){
		$drspagedata['dr_name']      = $_POST['dr_name'];
		$drspagedata['dr_specialty'] = $_POST['dr_specialty'];
		$drspagedata['sub_num']      = $_POST['sub_num'];
		try {
			include '../models/general.php';
			include '../models/insert.php';
			$object = new InsertInto($drspagedata, 'doctors');
			if($object == TRUE) echo "<h1>Done Operation</h1>";
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}else include 'views/doctorspage.php';
?>