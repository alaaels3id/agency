<h1>Exams Page</h1>
<?php 
	if(isset($_POST['submit']) && $_POST['submit'] == "Save"){
		//questions form inputs
		try {
			$exams_page_questions_data['question'] = $_POST['question'];
			$exams_page_questions_data['c1'] = $_POST['c1'];
			$exams_page_questions_data['c2'] = $_POST['c2'];
			$exams_page_questions_data['c3'] = $_POST['c3'];
			$exams_page_questions_data['c4'] = $_POST['c4'];
			
			include '../models/general.php';
			include '../models/insert.php';
			
			$object = new InsertInto($exams_page_questions_data, 'questions');
			if($object == TRUE) echo "<h1>Question has been added Successfully</h1>";
		
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}else include 'views/Addexams.php';
?>