<?php 
	include '../models/database.php';
	$obj = new Database('../models/info.php');
	$conn = $obj->Connect();
	include '../php/exam.php'; 
?>
<section>
	<div class="row">
		<div class="col-md-12 admin-aboutpage">
			 <?php
                while($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)){
                    echo "<h1>Exam subject is :<mark>".ucfirst($row['exam_subject'])."</mark></h1>";
                }
            ?>
			<form method="POST">
				<div class="form-group">
			  		<h1>MCQ</h1>
			    	<input type="text" name="question" class="form-control input-lg" placeholder="The Question"><br>

			    	<input style="width: 114px; display: inline;" type="text" name="c1" class="form-control input-lg" placeholder="1st">
			    	<input style="width: 114px; display: inline;" type="text" name="c2" class="form-control input-lg" placeholder="2nd">
			    	<input style="width: 114px; display: inline;" type="text" name="c3" class="form-control input-lg" placeholder="3rd">
			    	<input style="width: 114px; display: inline;" type="text" name="c4" class="form-control input-lg" placeholder="4th">
			  	</div>
			  	<input type="submit" name="submit" class="btn btn-lg btn-info" value="Save">
			</form>
		</div>
	</div>
</section>