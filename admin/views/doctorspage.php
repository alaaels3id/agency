<section>
	<div class="row">
		<div class="col-md-12 admin-aboutpage">
			<form method="POST">
				<div class="form-group">
			    	<input type="text" name="dr_name" class="form-control input-lg" placeholder="Doctor Name">
			  	</div>
				<div class="form-group">
			    	<input type="text" name="dr_specialty" class="form-control input-lg" placeholder="Dr. Specialty">
			  	</div>
			  	<div class="form-group">
			    	<input type="number" name="sub_num" class="form-control input-lg" placeholder="Subjects Number">
			  	</div>
			  	<input type="submit" name="submit" class="btn btn-lg btn-info" value="Save">
			</form>
		</div>
	</div>
</section>