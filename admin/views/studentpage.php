<?php 
	include '../models/database.php';
	$obj = new Database('../models/info.php');
	$conn = $obj->Connect();
	$q = "SELECT `username` FROM `users`";
	$sql = mysqli_query($conn, $q);
?>
<section>
	<div class="row">
		<div class="col-md-12 admin-aboutpage">
			<form method="POST">
				<div class="form-group">
			    	<select name="stu_name" class="input-lg" style="width: 350px;">
			    		<?php 
			    			while($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)){
			    				echo "<option value=\"{$row['username']}\">".ucfirst($row['username'])."</option>";
			    			}
			    		?>
			    	</select>
			  	</div>
			  	<div class="form-group">
			    	<input type="text" name="stu_grade" class="form-control input-lg" placeholder="Enter the Student's Grade">
			  	</div>
			  	<div class="form-group">
			    	<input type="number" name="stu_deg" class="form-control input-lg" placeholder="Enter the Student's Total Degrees">
			  	</div>
			  	<input type="submit" name="submit" class="btn btn-lg btn-info" value="Save">
			</form>
		</div>
	</div>
</section>