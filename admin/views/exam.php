<section>
	<div class="row">
		<div class="col-md-12 admin-aboutpage">
			<form method="POST">
				<div class="form-group">
					<label for="examdatestart">Exam Date</label>
			    	<input type="date" class="form-control input-lg" id="examdatestart" name="exam_date"/>
			  	</div>
			  	<div class="form-group">
			  		<label for="examtype">Exam Type</label><br>
			    	<select name="exam_type" style="width: 350px;" class="input-lg">
			    		<option value="final">Final</option>
			    		<option value="mid-term">Mid-Term</option>
			    	</select>
			  	</div>
			  	<div class="form-group">
			  		<label for="examsubjects">Subject Name</label>
			    	<input type="text" class="form-control input-lg" id="examsubjects" name="exam_subject"/>
			  	</div>
			  	<input type="submit" name="submit" class="btn btn-lg btn-info" value="Save">
			</form>
		</div>
	</div>
</section>