<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavBarLinks">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="/agency/">Cairo High Institute</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="NavBarLinks">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"><a href="/agency/"></a></li>
                <li><a href="/agency/student">Student</a></li>
                <li><a href="/agency/Doctors">Doctors</a></li>
                <li><a href="/agency/terms">Terms</a></li>
                <li><a href="/agency/about">About Us</a></li>
                <li><a href="/agency/contact-us">Contact Us</a></li>
                <li><a style="cursor: pointer;" onclick="window.location.href='/agency/inc/userLogout.php'">Logout</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>