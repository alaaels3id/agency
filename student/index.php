<?php
    session_start();
    if(!isset($_SESSION['username'])){
        include '../inc/controller.php';
        die();
    }
?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Student</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet'>

        <!-- Theme CSS -->
        <link href="../css/agency.css" rel="stylesheet">
        <style>
            table thead tr th{ font-weight: bolder; font-size: 20px; color: #fec503;}
            table tbody tr td{ font-weight: bold; font-size: 20px;}
        </style>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <!-- Navbar Section -->
        <?php include '../inc/nav.php'; ?>

        <!-- Header -->
        <header>
            <div class="container">
                <div class="intro-text">
                    <div class="intro-lead-in">Welcome to CHI System!</div>
                    <div class="intro-heading">It's Nice To Meet You</div>
                    <a href="#student" class="page-scroll btn btn-xl">Tell Me More</a>
                </div>
            </div>
        </header>

        <!-- About Section -->
        <section id="student">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">About</h2>
                        <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php include '../php/student.php'; ?> 
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Names</th>
                                    <th>Grades</th>
                                    <th>Dregrees</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                while($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)){
                                    if($row['stu_name'] == $_SESSION['username']){
                                        echo "<tr>";
                                        echo "<td class=\"tdata\">".$row['id']."</td>";
                                        echo "<td class=\"tdata\">".ucfirst($row['stu_name'])."</td>";
                                        echo "<td class=\"tdata\">".$row['stu_grade']."</td>";
                                        echo "<td class=\"tdata\">".$row['stu_deg']."</td>";
                                        echo "</tr>";
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <?php 
                            include '../php/exam.php';
                            while($row = mysqli_fetch_array($sql, MYSQLI_ASSOC))
                            {
                                echo "<p class=\"section-subheading text-muted\" style=\"font-size: 15px;\">";
                                echo "The Next <span class=\"text-warning\"><b>{$row['exam_type']}</b></span> Examination ";
                                echo "Will be in <span class=\"text-warning\"><b>{$row['exam_date']}</b></span>";
                                echo " The subject is : <span class=\"text-warning\"><b>{$row['exam_subject']}</b></span>";
                                echo "</p>";
                            }
                        ?>
                        <a href="../exams/">Online Exames</a>
                    </div>
                </div>
            </div>
        </section>
        <?php include '../inc/footer.php'; ?>
        <?php include '../inc/scripts2.php'; ?>
    </body>
</html>