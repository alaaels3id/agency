<?php 
	include './models/database.php';
	$obj = new Database('./models/info.php');
	$conn = $obj->Connect();
	$query = "SELECT * FROM `home`";
	$sql = @mysqli_query($conn, $query);
	while($row = @mysqli_fetch_array($sql, MYSQLI_ASSOC))
    {
        $filname = rawurlencode($row['lec_name']);
        echo "<div class=\"col-md-4 col-sm-6 portfolio-item\">";
            echo "<a href=\"admin/uploads/{$filname}\" class=\"portfolio-link\">";
                echo "<div class=\"portfolio-hover\"></div>";
                echo "<img src=\"img/portfolio/roundicons.png\" class=\"img-responsive\" alt=\"\">";
            echo "</a>";
            echo "<div class=\"portfolio-caption\">";
                echo "<h4>{$row['file_name']}</h4>";
                echo "<p class=\"text-muted\">{$row['discription']}</p>";
            echo "</div>";
        echo "</div>";
    }
	if(!$sql) echo "Error : Try Again";

?>