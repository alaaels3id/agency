<?php
    session_start();
    if(!isset($_SESSION['username'])){
        include '../inc/controller.php';
        die();
    }
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Doctors</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet'>

    <!-- Theme CSS -->
    <link href="../css/agency.css" rel="stylesheet">
    <style type="text/css">
    table thead tr th{ font-weight: bolder; font-size: 20px; color: #fec503;}
    table tbody tr td{ font-weight: bold; font-size: 15px;}
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body class="index">
    <?php include '../inc/nav.php'; ?>
    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome to CHI System!</div>
                <div class="intro-heading">It's Nice To Meet You</div>
                <a href="#doctors" class="page-scroll btn btn-xl">Tell Me More</a>
            </div>
        </div>
    </header>
    <!-- Portfolio Grid Section -->
    <section id="doctors" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Doctors</h2>
                    <h3 class="section-subheading text-muted">This is All the Doctors Page</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php include '../php/doctors.php'; ?>
                     <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Names</th>
                                <th>Dr. Specialty</th>
                                <th>Number of Subjects</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            while($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)){
                                echo "<tr>";
                                echo "<td>{$row['id']}</td>";
                                echo "<td>".ucfirst($row['dr_name'])."</td>";
                                echo "<td>{$row['dr_specialty']}</td>";
                                echo "<td>{$row['sub_num']}&ensp;&ensp;Subject</td>";
                                echo "</tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <?php include '../inc/footer.php'; ?>
    <?php include '../inc/scripts2.php'; ?>
</body>
</html>
