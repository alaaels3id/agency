<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Terms</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet'>

        <!-- Theme CSS -->
        <link href="../css/agency.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <!-- Navbar Section -->
        <?php include '../inc/nav.php'; ?>

        <!-- Header -->
        <header>
            <div class="container">
                <div class="intro-text">
                    <div class="intro-lead-in">Welcome to CHI System!</div>
                    <div class="intro-heading">It's Nice To Meet You</div>
                    <a href="#terms" class="page-scroll btn btn-xl">Tell Me More</a>
                </div>
            </div>
        </header>

        <!-- About Section -->
        <section id="terms">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Terms</h2>
                        <h3 class="section-subheading text-muted">
                            However, You enter this website.you accept our all terms
                             and conditions..</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="timeline">
                            <li>
                                <div class="timeline-image">
                                    <img class="img-circle img-responsive" src="../img/about/1.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>1th</h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p class="text-muted">
                                            <p class="text-muted h3">this website is stablished for CHI Students only</p>
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image">
                                    <img class="img-circle img-responsive" src="../img/about/2.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>2ed</h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p class="text-muted h3">and it is forbidden to any one to use it.</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="timeline-image">
                                    <img class="img-circle img-responsive" src="../img/about/3.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>3th</h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p class="text-muted h3">the student use this site for study only not for anything else</p>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image">
                                    <img class="img-circle img-responsive" src="../img/about/4.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>4th</h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p class="text-muted h3">the mangers of this site is not responsable for any fake information that published on it</p>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image">
                                    <h4>Be Part<br>Of Our<br>Story!</h4>
                                </div>
                            </li>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include '../inc/footer.php'; ?>
        <?php include '../inc/scripts2.php'; ?>
    </body>
</html>